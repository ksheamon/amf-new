<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  STARTERKIT_preprocess_html($variables, $hook);
  STARTERKIT_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_page(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // STARTERKIT_preprocess_node_page() or STARTERKIT_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */

function amf_preprocess_page(&$variables){
  $variables['custom_menu'] = menu_navigation_links('menu-custom-menu');
}

function amf_links__menu_custom_menu(&$variables){
	$class = '';	
	$classes = $variables['attributes']['class'];
	foreach($classes as $c){
		$class .= $c .' ';
	}
	$output = '<ul class="'.$class.'">';
	$links = $variables['links'];
	$type = $variables['type'];
	switch($type){
		case 'logo':
			foreach($links as $k => $v){
				$t = str_replace(' ', '', $v['title']);
				$t = str_replace('\'', '', $t);
				$output .= '<a href="/'.$v['href'].'"><li class="'.$k.' '.$t.' logoimg">'.$v['title'].'</li></a>';
			}
		break;
		case 'main':
		case 'super':
		case 'foot':
		default:
			$site_frontpage = variable_get('site_frontpage', 'node');		
			$i = 1;
			$current = menu_get_item();			
			foreach($links as $k => $v){
				$k = $k;
				if($v['href'] == '<front>'){
					$output .= '<li class="'.$k.' logo"><a href="/'.$site_frontpage.'"><img src="'.theme_get_setting('logo') .'"/></a></li>';
				}else{
					if($v['title'] == "Pinsider's Club"){
						$output .= '<li class="'.$k.'"><a href="/'.$v['href'].'">Pinsider\'s <span class="thirsty">Club</span></a></li>';
					}else{
						if($i == 1){
							$k .= ' first';
						} else if($i == count($links)){
							$k .= ' last';
						}
						if($current['href'] == $v['href']){
							$k .= ' active';
						}
						$output .= '<li class="'.$k.'"><a href="/'.$v['href'].'">'.$v['title'].'</a><span></span></li>';
					}
				}
				$i ++;
			}			
		break;
	}		
	$output .= '</ul>';	
	return $output;	
}


function amf_links__custom_mobile_menu(&$variables){
	$main = $variables['main'];
	$bottom = $variables['secondary'];		
	$output = '<ul>';
	$c = '';
	foreach($main as $k => $v){
		if($v['href'] !== '<front>'){
			if($v['title'] == 'Find A Location'){
				$c = 'yellow';
			}
			$output .= '<a href="/'.$v['href'].'" class="'.$c.'"><li>'.$v['title'].'</li><span class="navarrow"></span></a>';
		}		
	}
	foreach($bottom as $k => $v){
		if($v['title'] == "Pinsider's Club"){
			$v['title'] = 'Pinsider\'s <span class="thirsty">Club</span>';
		}
		$output .= '<a href="/'.$v['href'].'" class="black"><li>'.$v['title'].'</li><span class="navarrow"></span></a>';
	}
	$output .= '</ul>';
	return $output;	
}