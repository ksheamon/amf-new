<?php
/**
 * @file
 * Returns the HTML for the footer region.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728140
 */
 $logo_menu = menu_navigation_links('menu-secondary-menu');
 $link_menu = menu_navigation_links('menu-link-menu');
 $footer_left = menu_navigation_links('menu-footer-left-menu');
?>
<div class="bottom-wrap">
  <footer id="footer" class="footer navigation container">
		<div class="foot top">
			<a href="javascript:void(0);" class="viewall">Links</a>
			<nav class="alignleft left">
				<?php		
				print theme('links__menu_custom_menu', array(
					'type' => 'foot',
					'links' => $footer_left,
					'attributes' => array(
						'class' => array('links', 'inline', 'clearfix'),
					)
					)
				); ?>
			</nav>
			<nav class="right">
				<?php		
				print theme('links__menu_custom_menu', array(
					'type' => 'foot',
					'links' => $link_menu,
					'attributes' => array(
						'class' => array('links', 'inline', 'clearfix'),
					)
					)
				); ?>
			</nav>
		</div>
		<div class="foot bottom">
			<nav class="left">
				<?php
				/*Supernav Left Logos*/				
				print theme('links__menu_custom_menu', array(
					'type' => 'logo',
					'links' => $logo_menu,
					'attributes' => array(
						'class' => array('links', 'inline', 'clearfix'),
					)
					)
				); ?>
			</nav>
			<nav class="right copyright">
				&copy; <?php echo date('Y');?> Bowlmor AMF Centers. All rights reserved.
			</nav>
		</div>
  </footer>
</div>

