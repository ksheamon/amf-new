<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div id="page">
		<?php  print render($page['navigation']); ?>	
	<div id="main">
		<div id="content" class="column" role="main">
			<?php if ($title): ?>
				<h1 class="page__title title" id="page-title"><?php print $title; ?></h1>
			<?php endif; ?>
			<?php print render($page['content']); ?>
		</div>
	</div>
	
</div>
<?php  print render($page['footer']); ?>
<?php print render($page['bottom']); ?>
