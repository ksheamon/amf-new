<?php
/**
 * @file
 * Returns the HTML for the bottom region.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728140
 */
?>
<div class="greybg">
	<div class="container">
		<div class="left">
			<div class="pinsider">
				<div class="pin-logo"><img src="<?php echo drupal_get_path('theme',$GLOBALS['theme'])?>/images/pinsider.png"/></div>
				<div class="pinform">
					<div class="text">Enjoy exclusive offers, discounts, &amp; more. <a>Learn More</a></div>
					<form>
						<input type="text" name="user_email" placeholder="Enter your email address" />
						<input type="submit" class="desktop" value="Join the club"/>
						<input type="submit" class="mobile" value="Join"/>
					</form>
				</div>
			</div>
			<div class="roll-social">
				<div class="text">
					ROLL <span class="thirsty">Social</span>
				</div>
				<div class="icons">
					<div class="fb"></div>
					<div class="twitter"></div>
					<div class="yt"></div>
					<div class="insta"></div>
				</div>
			</div>
		</div>
		
	</div>
</div>


