<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>
 <script type="text/javascript">
    var slide;
    jQuery(window).load(function(){
        slide = new bfmSlider({
        "effect": "slide",
        "interval": 4000,
        "speed": 600,
        "stopOnChange": true,
        "start": 0,
        "wrapper": jQuery('#slide')
      },{
        "direction": "h",
        "wrapper": jQuery('#slide_thumbnails')
      });      
    });
  </script>
<div id="page" class="top">
	<?php  print render($page['navigation']); ?>	
	<div id="main" class="container front-page">
		<div id="content" class="header" role="main">
			<div class="text">
				<h1 class="thirsty">You're Up</h1>
				<div class="down-arrow"></div>
				<p class="subtext">Find an AMF near you</p>
			</div>
			<div class="location-form">
				<form>
					<div class="form-inputs">
						<div class="left">
							<input type="text" name="zipcode" placeholder="Enter Your City and State or ZIP Code"/>
						</div>
						<span class="thirsty or">or</span>
						<div class="use-loc"><input type="checkbox" name="use-loc"/>Use My Current Location</div>
					</div>
					<div class="submit yellow loc-btn"><span class="icn"></span>Find A Location</div>
				</form>
			</div>
		</div>
	</div>	
	<div class="specials-slider">
		<div id="slide" class="slider container">
			<ul>
			<?php
				for($i=1; $i<=4; $i++){
					echo '<li>';
					echo '<div class="left">Slide '.$i.' Left</div>';
					echo '<div class="right">Slide '.$i.' Right</div>';
					echo '</li>';
				}
			?>
			</ul>
		</div>
		<div id="slide_thumbnails" class="slide_thumbnails slider">
			<div class="prev"></div>
			<div class="next"></div>
			<ul>
				<?php
					for($i=1; $i<=4; $i++){
						$n = sprintf('%1$02d', $i);
						echo '<li>';
							echo $i;
						echo '</li>';
					}
				?>
			</ul>
		</div>  
	</div>
	<div class="darrow">
		<div class="white"></div>
	</div>
</div>
<div class="wood rel">
	<div class="container">
		Go Ahead, Pick a Lane!
	</div>
	<div class="darrow">
		<div class="black"></div>
	</div>
</div>
<div class="homequote rel">
	<div class="container">
		<div class="text">
			Check your inhibitions at the door &amp;
			<span class="thirsty">just bowl</span>
		</div>
	</div>
	<div class="darrow">
		<div class="white"></div>
	</div>
</div>
<div class="wood rel">
	<div class="darrow">
		<div class="black"></div>
	</div>
</div>
<?php print render($page['pinsider_bottom']); ?>
<?php  print render($page['footer']); ?>

