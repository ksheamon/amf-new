<?php
/**
 * @file
 * Returns the HTML for the footer region.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728140
 */
 $logo_menu = menu_navigation_links('menu-secondary-menu');
 $main_menu = menu_navigation_links('main-menu');
 $secondary_menu = menu_navigation_links('menu-supernav-right');
?>

<div class="nav-menu desktop container">
	<div class="navigation inblock" id="supernav" role="banner">
		<nav class="alignleft logo-menu"  role="navigation">
			<?php
			/*Supernav Left Logos*/				
			print theme('links__menu_custom_menu', array(
				'type' => 'logo',
				'links' => $logo_menu,
				'attributes' => array(
					'class' => array('links', 'inline', 'clearfix'),
				)
				)
			); ?>
		</nav>
		<nav class="header__secondary-menu alignright" id="secondary-menu" role="navigation">
		<?php 					
			print theme('links__menu_custom_menu', array(
				'type' => 'super',
				'links' => $secondary_menu,
				'attributes' => array(
				'class' => array('links', 'inline', 'clearfix'),
			  )
			)); ?>
		</nav>
	</div>
	<div id="mainnav" class="navigation  block">			
		<nav id="main-menu" role="navigation" tabindex="-1">
		<?php
			print theme('links__menu_custom_menu', array(
				'type' => 'main',
				'links' => $main_menu,
				'attributes' => array(
					'class' => array('links', 'inline', 'clearfix', 'aligncenter'),
				)
			));
		?>
		</nav>
	</div>
</div>

<div class="nav-menu mobile">
	<div class="container">
	<img src="<?php echo drupal_get_path('theme', $GLOBALS['theme']) . theme_get_setting('logo_sm');?>"/>
	<div class="alignright mobile-btns">
		<div class="menu">
			<div id="menu-btn" class="menu-btn">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="arrow"></div>
		</div>
		<div class="locations">L</div>
	</div>	
	</div>
	<div class="responsive-menu">
		<?php 					
			print theme('links__custom_mobile_menu', array(
				'main' => $main_menu,
				'secondary' => $secondary_menu,
				'attributes' => array(
				'class' => array('links', 'inline', 'clearfix'),
			  )
			)); ?>
	</div>
</div>